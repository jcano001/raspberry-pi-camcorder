# README #

## camcorder.py ##
Python script to record video clips to an external mounted USB NTFS hard drive.
Use time.sleep() record for desired amount of time

## time-lapse.py ##
Python script to capture time-lapse images to an external mounted USB NTFS hard drive.
Use time.sleep() for delay between frames

## External PiCam resources: ##
* http://www.raspberrypi.org/documentation/usage/camera/python/README.md
* http://www.raspberrypi.org/learning/python-picamera-setup/